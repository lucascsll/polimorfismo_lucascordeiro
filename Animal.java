public abstract class Animal implements ICriptografavel{
    private int qtdPe;
    private boolean carnivora;
    private boolean herbivaro;
    
    public int getQtdPe() {
        return qtdPe;
    }

    public void setQtdPe(int qtdPe) {
        this.qtdPe = qtdPe;
    }

    public boolean isCarnivora() {
        return carnivora;
    }

    public void setCarnivora(boolean carnivora) {
        this.carnivora = carnivora;
    }

    public boolean isHerbivaro() {
        return herbivaro;
    }

    public void setHerbivaro(boolean herbivaro) {
        this.herbivaro = herbivaro;
    }
    

    
}
