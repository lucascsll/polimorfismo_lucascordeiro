public class Humano extends Animal {

    private String nome;
    private int idade;
    private int chaveCript;

    public Humano(String nome, int idade, int chaveCript) {
        this.nome = nome;
        this.idade = idade;
        this.chaveCript = chaveCript;
    }

    public String getNome() {
        return nome;
    }

    public int getIdade() {
        return idade;
    }

    public void criptografar() {
        String alfabetoCriptografado = getAlfabetoCriptografado();
        String nomeCriptografado = "";
        for(int i = 0 ; i < this.nome.length() ; i++){
            char charAtualNome = this.nome.charAt(i);
            
            int posicao = 0;
            for(int j = 0 ; j < chaveCriptografia.length() ; j++){
                char charSemCriptografia = chaveCriptografia.charAt(j);
                
                if(charAtualNome == charSemCriptografia){
                    posicao = j;
                }
            }
            nomeCriptografado += alfabetoCriptografado.charAt(posicao);
        }
        this.nome = nomeCriptografado;
        this.idade += getChaveLimitada();
    }

    public void descriptografar() {
        String alfabetoCriptografado = getAlfabetoCriptografado();
        String nomeDescriptografar = "";
        for(int i = 0 ; i < this.nome.length() ; i++){
            char charAtualNomeCriptografado = this.nome.charAt(i);
            
            int posicao = 0;
            for(int j = 0 ; j < alfabetoCriptografado.length() ; j++){
                char charComCriptografia = alfabetoCriptografado.charAt(j);
                
                if(charAtualNomeCriptografado == charComCriptografia){
                    posicao = j;
                }
            }
            nomeDescriptografar += chaveCriptografia.charAt(posicao);
        }
        this.nome = nomeDescriptografar;
        this.idade -= getChaveLimitada();
    }
    private String getAlfabetoCriptografado() {
        int chaveLimitada = getChaveLimitada();
        
        String nomeCriptografado = "";
        for (int i = 0; i < chaveCriptografia.length(); i++) {
            int posicao = i + chaveLimitada;
            if (posicao >= chaveCriptografia.length()) {
                posicao -= chaveCriptografia.length();
            }
            nomeCriptografado += chaveCriptografia.charAt(posicao);
        }
        return nomeCriptografado;
    }

    private int getChaveLimitada() {
        int chaveLimitada = this.chaveCript;
        while (chaveLimitada >= chaveCriptografia.length()) {
            chaveLimitada -= chaveCriptografia.length();
        }
        return chaveLimitada;
    }
    
}
